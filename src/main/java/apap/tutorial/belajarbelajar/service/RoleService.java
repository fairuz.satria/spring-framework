package apap.tutorial.belajarbelajar.service;

import java.util.List;

import apap.tutorial.belajarbelajar.model.RoleModel;

public interface RoleService {

    List<RoleModel> findAll();

    RoleModel getById(Long valueOf);

}

