package apap.tutorial.belajarbelajar.service;

import apap.tutorial.belajarbelajar.model.PengajarModel;
import java.util.List;

public interface PengajarRestService {
    PengajarModel createPengajar(PengajarModel pengajar);
    List<PengajarModel> retrieveListPengajar();
    PengajarModel getPengajarByNoPengajar(Long noPengajar);
    PengajarModel updatePengajar(Long noPengajar, PengajarModel pengajarUpdate);
    void deletePengajar(Long noPengajar);
    PengajarModel predictGender(Long noPengajar);
}
