package apap.tutorial.belajarbelajar.service;
import apap.tutorial.belajarbelajar.model.PengajarModel;

public interface PengajarService {
    void addPengajar(PengajarModel pengajar);
    public PengajarModel getPengajarByNo(Long code);
    public PengajarModel updatePengajar(PengajarModel pengajar);
    public PengajarModel deletePengajar(PengajarModel pengajar);
    boolean checkIfExist(String noPengajar);
}
