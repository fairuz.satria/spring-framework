package apap.tutorial.belajarbelajar.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import javax.transaction.Transactional;

import apap.tutorial.belajarbelajar.repository.PengajarDB;
import reactor.core.publisher.Mono;
import apap.tutorial.belajarbelajar.model.PengajarModel;
import apap.tutorial.belajarbelajar.model.CourseModel;
import apap.tutorial.belajarbelajar.rest.GenderizeDetail;
import apap.tutorial.belajarbelajar.rest.Setting;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriBuilder;

@Service
@Transactional
public class PengajarRestServiceImpl implements PengajarRestService {
    private final WebClient webClient;
    
    @Autowired
    private PengajarDB pengajarDb;

    public PengajarRestServiceImpl(WebClient.Builder webClientBuilder) {
        this.webClient = webClientBuilder.baseUrl(Setting.genderizeUrl).build();
    }

    @Override
    public PengajarModel createPengajar(PengajarModel pengajar){
        return pengajarDb.save(pengajar);
    }

    @Override
    public List<PengajarModel> retrieveListPengajar() {
        return pengajarDb.findAll();
    }

    @Override
    public PengajarModel getPengajarByNoPengajar(Long noPengajar){
        Optional<PengajarModel> pengajar = pengajarDb.findByNoPengajar(noPengajar);
        if(pengajar.isPresent()) {
            return pengajar.get();
        } else {
            throw new NoSuchElementException();
        }
    }

    @Override
    public PengajarModel updatePengajar(Long noPengajar, PengajarModel pengajarUpdate){
        PengajarModel pengajar = getPengajarByNoPengajar(noPengajar);
        pengajar.setIsPengajarUniversitas(pengajarUpdate.getIsPengajarUniversitas());
        pengajar.setNamaPengajar(pengajarUpdate.getNamaPengajar());
        return pengajarDb.save(pengajar);
    }

    @Override
    public void deletePengajar(Long noPengajar){
        PengajarModel pengajar = getPengajarByNoPengajar(noPengajar);
        CourseModel course = pengajar.getCourse();
        if(isClosed(course.getTanggalDimulai(), course.getTanggalBerakhir())){
            pengajarDb.delete(pengajar);
        } else {
            throw new UnsupportedOperationException();
        }
    }

    public boolean isClosed(LocalDateTime tanggalDimulai, LocalDateTime tanggalBerakhir){
        LocalDateTime now = LocalDateTime.now();
        if(now.isBefore(tanggalDimulai) || now.isAfter(tanggalBerakhir)){
            return true;
        }
        return false;
    }

    @Override
    public PengajarModel predictGender(Long noPengajar){
        PengajarModel pengajar = getPengajarByNoPengajar(noPengajar);
        String namaDepanPengajar = pengajar.getNamaPengajar().split(" ")[0];
        Mono<GenderizeDetail> responseAPIGenderize = webClient
        .get()
        .uri("/?name="+namaDepanPengajar)
        .retrieve().bodyToMono(GenderizeDetail.class);
        boolean genderBool = responseAPIGenderize.block().getGender().equals("female") ? true : false;
        pengajar.setJenisKelamin(genderBool);
        return updatePengajar(noPengajar, pengajar);
    
    }
}
