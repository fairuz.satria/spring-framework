package apap.tutorial.belajarbelajar.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import apap.tutorial.belajarbelajar.model.UserModel;
import apap.tutorial.belajarbelajar.repository.UserDb;

@Service
public class UserServiceImpl implements UserService {
  @Autowired
  private UserDb userDb;

  @Override
  public UserModel addUser(UserModel user) {
    String pass = encrypt(user.getPassword());
    user.setPassword(pass);
    return userDb.save(user);
  }

  @Override
  public String encrypt(String password) {
    BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
    String hashedPassword = passwordEncoder.encode(password);
    return hashedPassword;
  }

  @Override
  public UserModel getUserByUsername(String username) {
    return userDb.findByUsername(username);
  }

  @Override
  public List<UserModel> getListUser(){
    return userDb.findAll();
  }

  @Override
  public void deleteUser(UserModel user){
    userDb.delete(user);
  }

  @Override
  public Boolean checkIfValidOldPassword(UserModel user, String password) {
      return new BCryptPasswordEncoder().matches(password, user.getPassword());
  }

  @Override
  public void changePassword(UserModel user, String password) {
      String hashedPassword = encrypt(password);
      user.setPassword(hashedPassword);
      userDb.save(user);
  }

  @Override
  public Boolean checkIfValidNewPassword(String password) {
    String huruf = ".*[A-Za-z].*";
    String angka = ".*[0-9].*";
    String simbol = ".*[!@#$%&*()_+=|<>?{}\\[\\]~-].*";
    if (password.length() < 8 || !password.matches(huruf) || !password.matches(angka)
      || !password.matches(simbol)) {
      return false;
    }else{
      return true;
    }
  }

}
