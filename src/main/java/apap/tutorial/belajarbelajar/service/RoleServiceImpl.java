
package apap.tutorial.belajarbelajar.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import apap.tutorial.belajarbelajar.model.RoleModel;
import apap.tutorial.belajarbelajar.repository.RoleDb;

@Service
public class RoleServiceImpl implements RoleService {
  @Autowired
  private RoleDb roleDb;

  @Override
  public List<RoleModel> findAll() {
    return roleDb.findAll();
  }

  @Override
  public RoleModel getById(Long valueOf){
    Optional<RoleModel> role = roleDb.findById(valueOf);

    if (role.isPresent()){
        return role.get();
    } else return null;
  }
  
}

