package apap.tutorial.belajarbelajar.service;

import java.util.List;

import apap.tutorial.belajarbelajar.model.UserModel;

public interface UserService {
  UserModel addUser(UserModel user);
  public String encrypt(String password);
  UserModel getUserByUsername(String username);
  List<UserModel> getListUser();
  void deleteUser(UserModel user);
  Boolean checkIfValidOldPassword(UserModel user, String password);
  void changePassword(UserModel user, String password);
  Boolean checkIfValidNewPassword(String password);
}

