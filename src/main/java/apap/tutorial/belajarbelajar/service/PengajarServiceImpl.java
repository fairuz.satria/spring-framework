package apap.tutorial.belajarbelajar.service;

import apap.tutorial.belajarbelajar.model.PengajarModel;
import apap.tutorial.belajarbelajar.repository.PengajarDB;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;

import java.util.Optional;

import javax.transaction.Transactional;

@Service
@Transactional
public class PengajarServiceImpl implements PengajarService{
    @Autowired
    PengajarDB pengajarDb;

    @Override
    public void addPengajar(PengajarModel pengajar){

        pengajarDb.save(pengajar);
    }

    @Override
    public boolean checkIfExist(String namaPengajar){
        return !pengajarDb.findByNamaPengajar(namaPengajar).isEmpty();
    }

    @Override
    public PengajarModel getPengajarByNo(Long noPengajar){
        Optional<PengajarModel> pengajar = pengajarDb.findByNoPengajar(noPengajar);

        if (pengajar.isPresent()){
            return pengajar.get();
        } else return null;
    }

    @Override
    public PengajarModel updatePengajar(PengajarModel pengajar){
        pengajarDb.save(pengajar);
        return pengajar;
    }

    @Override
    public PengajarModel deletePengajar(PengajarModel pengajar){
        pengajarDb.delete(pengajar);
        return pengajar;
    }
}
