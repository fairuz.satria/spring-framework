package apap.tutorial.belajarbelajar.restController;

import java.util.List;
import java.util.NoSuchElementException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import apap.tutorial.belajarbelajar.model.PengajarModel;
import apap.tutorial.belajarbelajar.service.PengajarRestService;

@RestController
@RequestMapping("/api/v1")
public class PengajarRestController {
    @Autowired
    private PengajarRestService pengajarRestService;

    @PostMapping(value = "/pengajar")
    private PengajarModel createPengajar(@Valid @RequestBody PengajarModel pengajar, BindingResult bindingResult){
        if(bindingResult.hasFieldErrors()){
            throw new ResponseStatusException(
                HttpStatus.BAD_REQUEST, "Request body has invalid type or missing field."
            );
        }else{
            return pengajarRestService.createPengajar(pengajar);
        }
    }

    @GetMapping(value = "/pengajar/{noPengajar}")
    private PengajarModel retrievePengajar(@PathVariable("noPengajar") Long noPengajar){
        try {
            return pengajarRestService.getPengajarByNoPengajar(noPengajar);
        } catch (NoSuchElementException e){
            throw new ResponseStatusException(
                HttpStatus.NOT_FOUND, "noPengajar pengajar " + noPengajar + " not found"
            );
        }
    }

    @DeleteMapping(value = "/pengajar/{noPengajar}")
    private ResponseEntity deletePengajar(@PathVariable("noPengajar") Long noPengajar){
        try {
            pengajarRestService.deletePengajar(noPengajar);
            return ResponseEntity.ok("Pengajar with noPengajar " + noPengajar + " has been deleted successfully");
        } catch (NoSuchElementException e){
            throw new ResponseStatusException(
                HttpStatus.NOT_FOUND, "noPengajar pengajar" + noPengajar + " not found"
            );
        } catch (UnsupportedOperationException e){
            throw new ResponseStatusException(
                HttpStatus.BAD_REQUEST, "Course is still open"
            );
        }
    }

    @PutMapping(value = "/pengajar/{noPengajar}")
    private PengajarModel updatePengajar(@PathVariable("noPengajar") Long noPengajar, @RequestBody PengajarModel pengajar){
        try {
            return pengajarRestService.updatePengajar(noPengajar, pengajar);
        } catch (Exception e) {
            // TODO: handle exception
            throw new ResponseStatusException(
                HttpStatus.NOT_FOUND, "noPengajar pengajar " + noPengajar + " not found"
            );
        }
    }

    @GetMapping(value = "/list-pengajar")
    private List<PengajarModel> retrieveListPengajar(){
        return pengajarRestService.retrieveListPengajar();
    }

    @GetMapping("/pengajar/jenis-kelamin/{noPengajar}")
    private PengajarModel setJenisKelaminGenderizeAPI(@PathVariable("noPengajar") Long noPengajar) {
        try {
            return pengajarRestService.predictGender(noPengajar);
        } catch (NoSuchElementException e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Nomor pengajar " + noPengajar.toString() + " not found"
            );
        } catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Course is still open"
            );
        }

    }
}
