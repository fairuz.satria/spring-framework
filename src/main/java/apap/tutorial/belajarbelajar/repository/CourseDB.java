package apap.tutorial.belajarbelajar.repository;

import apap.tutorial.belajarbelajar.model.CourseModel;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;
import java.util.Collection;
import java.util.List;

public interface CourseDB extends JpaRepository<CourseModel, String>{

    Optional<CourseModel> findByCode(String code);

    @Query("SElECT c FROM CourseModel c WHERE c.code = :code")
    Optional<CourseModel> findByCodeUsingQuery(@Param("code") String code);

    @Override
    @Query(value = "SELECT * FROM COURSE c ORDER BY c.name_course ASC", nativeQuery = true)
    List<CourseModel> findAll();
}
