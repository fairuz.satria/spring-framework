package apap.tutorial.belajarbelajar.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import apap.tutorial.belajarbelajar.model.RoleModel;

@Repository
public interface RoleDb extends JpaRepository<RoleModel, Long> {
  List<RoleModel> findAll();
  Optional<RoleModel> findById(Long valueOf);
}

