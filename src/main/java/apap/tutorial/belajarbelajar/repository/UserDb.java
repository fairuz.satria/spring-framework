package apap.tutorial.belajarbelajar.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import apap.tutorial.belajarbelajar.model.UserModel;

@Repository
public interface UserDb extends JpaRepository<UserModel, Long> {
  UserModel findByUsername(String username);
  List<UserModel> findAll();
}
