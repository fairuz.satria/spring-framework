package apap.tutorial.belajarbelajar.repository;

import apap.tutorial.belajarbelajar.model.PengajarModel;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

@Repository
public interface PengajarDB extends JpaRepository<PengajarModel, Long>{
    Optional<PengajarModel> findByNoPengajar(Long code);
    List<PengajarModel> findByNamaPengajar(String namaPengajar);
}
