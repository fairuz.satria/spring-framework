package apap.tutorial.belajarbelajar.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import apap.tutorial.belajarbelajar.model.RoleModel;
import apap.tutorial.belajarbelajar.model.UserModel;
import apap.tutorial.belajarbelajar.repository.UserDb;
import apap.tutorial.belajarbelajar.service.RoleService;
import apap.tutorial.belajarbelajar.service.UserService;

@Controller
@RequestMapping("/user")
public class UserController {
  @Autowired
  private UserService userService;

  @Autowired
  private RoleService roleService;

  @GetMapping(value="/gantiPassword/{username}")
  private String formGantiPassword(@PathVariable(value = "username") String username, Principal principal, Model model){
    if(principal.getName().equals(username)){
      model.addAttribute("username", username);
      return "form-update-user-password";
    }else{
      return "fail-permission";
    }
  }
/* 

  @RequestParam(value = "passwordlama") String passwordlama,
                                @RequestParam(value = "passwordbaru") String passwordbaru,
                                @RequestParam(value = "kpasswordbaru") String kpasswordbaru,
                                @RequestParam(value = "username") String username, */

  @PostMapping(value="/gantiPassword/{username}")
  private String submitGantiPassword( @RequestParam(value = "passwordlama") String passwordlama,
                                      @RequestParam(value = "passwordbaru") String passwordbaru,
                                      @RequestParam(value = "kpasswordbaru") String kpasswordbaru,
                                      @PathVariable(value = "username") String username,
                                      Principal principal, Model model) {
    if(principal.getName().equals(username)){
      UserModel user = userService.getUserByUsername(username);
      if(userService.checkIfValidOldPassword(user, passwordlama) && kpasswordbaru.equals(passwordbaru) && userService.checkIfValidNewPassword(passwordbaru)){
        userService.changePassword(user, passwordbaru);
        return "redirect:/";
      }else{
        model.addAttribute("message", String.valueOf(userService.checkIfValidOldPassword(user, passwordbaru)) +  String.valueOf(kpasswordbaru.equals(passwordbaru)) + String.valueOf(userService.checkIfValidNewPassword(passwordbaru)));
        return "fail-password";
      }
    }else{
      return "fail-permission";
    }
  }

  @GetMapping(value="/add")
  private String addUserFormPage(Principal principal, Model model) {
    UserModel userAuth = userService.getUserByUsername(principal.getName());
    if(userAuth.getRole().getRole().equals("Admin")){
      UserModel user = new UserModel();
      List<RoleModel> listRole = roleService.findAll();
      model.addAttribute("user", user);
      model.addAttribute("listRole", listRole);
      return "form-add-user";
    }else{
      return "fail-permission";
    }
  }

  @PostMapping(value="/add")
  private String addUserSubmit(@ModelAttribute UserModel user, Model model) {
    if(userService.checkIfValidNewPassword(user.getPassword())){
      user.setIsSso(false);
      userService.addUser(user);
      model.addAttribute("user", user);
      return "redirect:/";
    }else{
      return "fail-password";
    }
  }

  @GetMapping(value="/viewAll")
  public String listUser(Model model, Principal principal) {
    UserModel userAuth = userService.getUserByUsername(principal.getName());
    if(userAuth.getRole().getRole().equals("Admin")){
      //Mendapatkan semua CourseModel
      List<UserModel> listUser = userService.getListUser();

      //Add variabel semua courseModel ke "Listcourse" untuk dirender pada thymeleaf
      model.addAttribute("listUser", listUser);
      return "viewall-user";
    }else{
      return "fail-permission";
    }
  }

  
  @GetMapping(value="/viewAll/{username}/deleteuser")
  public String deleteUser(@PathVariable(value = "username")String username, Model model, RedirectAttributes redirectAttrs, Principal principal) {
      //Mendapatkan semua CourseModel
      UserModel userAuth = userService.getUserByUsername(principal.getName());
      if(userAuth.getRole().getRole().equals("Admin")){
        UserModel user = userService.getUserByUsername(username);
        userService.deleteUser(user);
        //Add variabel semua courseModel ke "Listcourse" untuk dirender pada thymeleaf
        redirectAttrs.addFlashAttribute("success",
            String.format("User baru berhasil disimpan "));
        return "redirect:/user/viewAll";
      }else{
        return "fail-permission";
      }
  }
}

