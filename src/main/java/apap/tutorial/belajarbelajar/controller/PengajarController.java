package apap.tutorial.belajarbelajar.controller;

import apap.tutorial.belajarbelajar.model.CourseModel;
import apap.tutorial.belajarbelajar.model.PengajarModel;
import apap.tutorial.belajarbelajar.service.CourseService;
import apap.tutorial.belajarbelajar.service.PengajarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import java.time.LocalDateTime;  

@Controller
public class PengajarController {
    @Qualifier("pengajarServiceImpl")
    @Autowired
    private PengajarService pengajarService;

    @Qualifier("courseServiceImpl")
    @Autowired
    private CourseService courseService;

    @GetMapping("/pengajar/add/{code}")
    public String addPengajarFormPage(@PathVariable String code, Model model) {
        PengajarModel pengajar = new PengajarModel();
        CourseModel course = courseService.getCourseByCodeCourse(code);
        pengajar.setCourse(course);
        model.addAttribute("pengajar", pengajar);
        return "form-add-pengajar";
    }

    @PostMapping("/pengajar/add")
    public String addPengajarSubmitPage(@ModelAttribute PengajarModel pengajar, Model model) {
        
        if(pengajarService.checkIfExist(pengajar.getNamaPengajar())){
            return "fail-add-pengajar";
        }else{
            pengajarService.addPengajar(pengajar);
            model.addAttribute("noPengajar", pengajar.getNoPengajar());
            return "add-pengajar";
        }
    }

    @GetMapping("/course/view/{code}/update/no_pengajar/{no_pengajar}")
    public String updatePengajarFormPage(@PathVariable(value = "code") String code,
                                        @PathVariable(value = "no_pengajar") Long noPengajar, Model model) {
        CourseModel course = courseService.getCourseByCodeCourse(code);
        PengajarModel pengajar = pengajarService.getPengajarByNo(noPengajar);
        model.addAttribute("pengajar", pengajar);
        model.addAttribute("course", course);
        return "form-update-pengajar";
    }

    @PostMapping("/course/view/{code}/update/no_pengajar/{no_pengajar}")
    public String updatePengajarFormPage(@PathVariable(value = "code") String code,
                                        @PathVariable(value = "no_pengajar") Long noPengajar,
                                        @ModelAttribute PengajarModel pengajar, Model model) {
        
        CourseModel course = courseService.getCourseByCodeCourse(code);
        if(LocalDateTime.now().isAfter(course.getTanggalBerakhir())){
            PengajarModel updatePengajar = pengajarService.updatePengajar(pengajar);
            model.addAttribute("no_pengajar", updatePengajar.getNoPengajar());
            model.addAttribute("nameCourse", course.getNameCourse());
            return "update-pengajar";      
        }else{
            return "fail-update-pengajar";
        }
     }

    @GetMapping("/course/view/{code}/delete/no_pengajar/{no_pengajar}")
    public String deletePengajar(@PathVariable(value = "code")String code,
                                    @PathVariable(value = "no_pengajar") Long noPengajar, Model model) {
        PengajarModel pengajar = pengajarService.getPengajarByNo(noPengajar);
        CourseModel course = courseService.getCourseByCodeCourse(code);
        if(courseService.isClosed(course.getTanggalDimulai(), course.getTanggalBerakhir())){
            pengajarService.deletePengajar(pengajar);
        
            model.addAttribute("code", course.getCode());
            model.addAttribute("namaPengajar", pengajar.getNamaPengajar());
            return "delete-pengajar";
        }else{
            return "fail-delete-pengajar";
        }

    }

    @PostMapping("/pengajar/delete")
    public String deletePengajarsubmit(
        @ModelAttribute CourseModel course,
        Model model
    ){
        if(courseService.isClosed(course.getTanggalDimulai(), course.getTanggalBerakhir())){
            for(PengajarModel pengajar :
            course.getListPengajar()){
                pengajarService.deletePengajar(pengajar);
            }
            model.addAttribute("code", course.getCode());
            return "delete-pengajar-all";
        }
        return "fail-delete-pengajar";
    }


}
