package apap.tutorial.belajarbelajar.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import apap.tutorial.belajarbelajar.model.UserModel;
import apap.tutorial.belajarbelajar.service.UserService;

import java.security.Principal;

@Controller
public class BaseController {

    @Autowired
    private UserService userService;

    @GetMapping("/")
    private String Home(Principal principal, Model model) {
        UserModel user = userService.getUserByUsername(principal.getName());
        model.addAttribute("user", user);
        return "home";
    }
}
