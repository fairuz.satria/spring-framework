package apap.tutorial.belajarbelajar.controller;

import apap.tutorial.belajarbelajar.model.CourseModel;
import apap.tutorial.belajarbelajar.model.PengajarModel;
import apap.tutorial.belajarbelajar.model.PenyelenggaraModel;
import apap.tutorial.belajarbelajar.service.CourseService;
import apap.tutorial.belajarbelajar.service.PenyelenggaraService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import java.time.LocalDateTime;  

import java.util.*;

@Controller
public class CourseController {
    @Qualifier("courseServiceImpl")
    @Autowired
    private CourseService courseService;

    @Autowired
    private PenyelenggaraService penyelenggaraService;

    //Routing URL yang diinginkan
    @GetMapping("/course/add")
    public String addCourseFormPage(Model model) {
        CourseModel course = new CourseModel();
        List<PenyelenggaraModel> listPenyelenggara = penyelenggaraService.getListPenyelenggara();
        List<PenyelenggaraModel> listPenyelenggaraNew = new ArrayList<>();
        List<PengajarModel> listPengajarNew = new ArrayList<>();

        course.setListPenyelenggara(listPenyelenggaraNew);
        course.setListPengajar(listPengajarNew);
        course.getListPenyelenggara().add(new PenyelenggaraModel());
        course.getListPengajar().add(new PengajarModel());

        model.addAttribute("course", course);
        model.addAttribute("listPenyelenggaraExisting", listPenyelenggara);
        return "form-add-course";
    }

    @PostMapping(value = "/course/add", params={"save"})
    public String addCourseSubmitPage(@ModelAttribute CourseModel course, Model model) {
        if (course.getListPenyelenggara() == null){
            course.setListPenyelenggara(new ArrayList<>());
        }

        List<PengajarModel> listPengajar = course.getListPengajar();
        for (PengajarModel pengajarModel : listPengajar) {
            pengajarModel.setCourse(course);
        }
        
        courseService.addCourse(course);
        System.out.println(course.getCode());
        model.addAttribute("code", course.getCode());
        return "add-course";
    }

    @PostMapping(value = "/course/add", params = {"addRow"})
    public String addRowPenyelenggaraMultiple(@ModelAttribute CourseModel course, Model model) {
        if(course.getListPenyelenggara() == null || course.getListPenyelenggara().size() == 0){
            course.setListPenyelenggara(new ArrayList<>());
        }
        course.getListPenyelenggara().add(new PenyelenggaraModel());
        List<PenyelenggaraModel> listPenyelenggara = penyelenggaraService.getListPenyelenggara();

        model.addAttribute("course", course);
        model.addAttribute("listPenyelenggaraExisting", listPenyelenggara);
        
        return "form-add-course";
    }

    @PostMapping(value = "/course/add", params = {"deleteRow"})
    public String deleteRowPenyelenggaraMultiple(@ModelAttribute CourseModel course, 
                                                 @RequestParam("deleteRow") Integer row,
                                                    Model model) {
        final Integer rowId = Integer.valueOf(row);
        course.getListPenyelenggara().remove(rowId.intValue());

        List<PenyelenggaraModel> listPenyelenggara = penyelenggaraService.getListPenyelenggara();

        model.addAttribute("course", course);
        model.addAttribute("listPenyelenggaraExisting", listPenyelenggara);
        
        return "form-add-course";
    }

    @PostMapping(value = "/course/add", params = {"addRowPengajar"})
    public String addRowPengajarMultiple(@ModelAttribute CourseModel course, Model model) {
        if(course.getListPengajar() == null || course.getListPengajar().size() == 0){
            course.setListPengajar(new ArrayList<>());
        }
        course.getListPengajar().add(new PengajarModel());

        model.addAttribute("course", course);
        model.addAttribute("listPenyelenggaraExisting", penyelenggaraService.getListPenyelenggara());
        
        return "form-add-course";
    }

    @PostMapping(value = "/course/add", params = {"deleteRowPengajar"})
    public String deleteRowPengajarMultiple(@ModelAttribute CourseModel course, 
                                                 @RequestParam("deleteRowPengajar") Integer row,
                                                    Model model) {
        final Integer rowId = Integer.valueOf(row);
        course.getListPengajar().remove(rowId.intValue());

        model.addAttribute("course", course);
        model.addAttribute("listPenyelenggaraExisting", penyelenggaraService.getListPenyelenggara());
        
        return "form-add-course";
    }

    @GetMapping("/course/viewAll")
    public String listCourse(Model model) {
        //Mendapatkan semua CourseModel
        List<CourseModel> listCourse = courseService.getListCourse();

        //Add variabel semua courseModel ke "Listcourse" untuk dirender pada thymeleaf
        model.addAttribute("listCourse", listCourse);
        return "viewall-course";
    }

    @GetMapping("/course/view")
    public String viewDetailCoursePage(@RequestParam(value = "code") String code, Model model){
        CourseModel course = courseService.getCourseByCodeCourse(code);

        if(course != null){
            List<PengajarModel> listPengajar = course.getListPengajar();
            List<PenyelenggaraModel> listPenyelenggara = course.getListPenyelenggara();
            model.addAttribute("listPengajar", listPengajar);
            model.addAttribute("course", course);
            model.addAttribute("listPenyelenggara", listPenyelenggara);
            return "view-course";
        }
        else{
            return "fail-view-course";
        }
    }

    @GetMapping("/course/view-query")
    public String viewDetailCoursePageQuery(@RequestParam(value = "code") String code, Model model){
        CourseModel course = courseService.getCourseByCodeCourse(code);
        List<PengajarModel> listPengajar = course.getListPengajar();
        model.addAttribute("listPengajar", listPengajar);
        model.addAttribute("course", course);
        return "view-course";
        
    }

    @GetMapping("/course/update/{code}")
    public String updateCourseFormPage(@PathVariable String code, Model model) {
        CourseModel course = courseService.getCourseByCodeCourse(code);
        model.addAttribute("course", course);
        return "form-update-course";
    }

    @PostMapping("/course/update/{code}")
    public String updateCourseFormPage(@ModelAttribute CourseModel course, Model model) {
        CourseModel updateCourse = courseService.updateCourse(course);
        model.addAttribute("code", updateCourse.getCode());
        return "update-course";
    }

    @GetMapping("/course/view/{code}/deletecourse")
    public String deleteCourse(@PathVariable(value = "code")String code, Model model) {
        CourseModel course = courseService.getCourseByCodeCourse(code);
        List<PengajarModel> listPengajar = course.getListPengajar();
        if(listPengajar.isEmpty() && (LocalDateTime.now().isAfter(course.getTanggalBerakhir())) || LocalDateTime.now().isBefore(course.getTanggalDimulai())){
            courseService.deleteCourse(course);
            model.addAttribute("code", course.getCode());
            return "delete-course";
        }else{
            return "fail-delete-course";
        }
        
    }

}
